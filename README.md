# QuickStart Angular

El motivo de este proyecto es enseñar lo básico para empezar a desarrollar en Angular.

## Herramientas:

 * Nodejs 12.18.1
 * Angular 9
 * Visual Studio
## Código:

La carpeta código se encuentra el código desarrollado en los videos

## Videos:

* Video 1:Instalación de herramientas de desarrollo:
	* Aprender a instalar Angular en Windows 10.
	* Aprender a instalar Nodejs en Windows 10.
    * Aprender a instalar Typescript en Windows 10.
    
* Video 2:Desarrollo de rutas:
	* Implementar rutas para una página básica. 
	
* Video 3:Desarrollo de servicios y parte grafica
	* Implementar servicios para una página básica.
	* Implementar parte gráfica
	
* Video 4:Potencial de Angular y oportunidades
	* Conocer las principales comunidades en angular.
	* Conocer las mejores plataformas para aprender angular.
	* Conocer las oportunidades de trabajo.

## Creador:

Sergio Miguel Dueñas Vera

sduenasv@ulasalle.edu.pe

